from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": str(city) + " " + str(state),
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["photos"][0]["src"]["original"]
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + " ," + state + " ," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": "5002e94145f9f876847f27b8c76fd28a",
        "units": "imperial",
    }
    response = requests.get(weather_url, params=params, headers=headers)
    content = json.loads(response.content)
    return content["main"]["temp"]
